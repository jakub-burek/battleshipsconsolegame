#pragma once
#if !defined(__PLAYER_H__)
#define  __PLAYER_H__

#include <string>
#include "Utilities.h"

class Game;

class IPlayer
{
public:
    virtual ~IPlayer() = default;

    virtual void setup() = 0;
    virtual void print(IPlayer& player) const = 0;
    virtual bool attack(IPlayer& player) = 0;

    virtual bool sinkShip(int x, int y) = 0;
    virtual Field getField(int x, int y) const = 0;
    virtual bool setField(int x, int y, Field field) = 0;

    virtual const std::string& getName() const = 0;;
    virtual bool isLost() const = 0;

    static IPlayer* CreateHumanPlayer(Game& game);
    static IPlayer* CreateComputerPlayer(Game& game);
};

#endif //__PLAYER_H__
#pragma once
#if !defined(__PLAYER_BASE_H__)
#define __PLAYER_BASE_H__

#include "Player.h"

class PlayerBase : public IPlayer
{
public:
    PlayerBase(Game& game);
    virtual ~PlayerBase() override = default;

    virtual void setup() override;
    virtual void print(IPlayer& player) const override;
    virtual bool attack(IPlayer& player) override;

    virtual bool sinkShip(int x, int y) override final;
    virtual Field getField(int x, int y) const override final;
    virtual bool setField(int x, int y, Field field) override final;

    bool setOneShip(int x, int y, int length, Direction direction);
    const std::string& getName() const;
    virtual bool isLost() const override final;

protected:
    void clearStart();
    Game& getGame() const;
    void setName(std::string name);
    const Field (&GetBoard() const)[10][10];

private:
    unsigned short fleetCount;
    std::string name;
    Game& game;
    Field Board[10][10];
};

#endif //__PLAYER_BASE_H__
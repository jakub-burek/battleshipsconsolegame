#include "Player_base.h"

class AIPlayer final : public PlayerBase
{
public:
    AIPlayer(Game& game);
    virtual ~AIPlayer() override final = default;

    virtual void setup() override final;
    virtual bool attack(IPlayer& player) override final;
};

//AI Player constructor
AIPlayer::AIPlayer(Game& game) :
    PlayerBase(game)
{
    setName("Computer");
}

//Method setting randomly AI Player's ships
void AIPlayer::setup()
{
	int x, y, d;
	Direction direction;

	for (int i = 5; i > 1; --i)
	{
		while (true)
		{
			x = rand() % 10;
			y = rand() % 10;
			d = rand() % 2;
			
            direction = (rand() % 2) ? Direction::Right : Direction::Up;

			if (setOneShip(x, y, i, direction) == true)
				break;
		}
	}
}

//Method randomly attacking enemy player
bool AIPlayer::attack(IPlayer& player)
{
	int x, y;
	while (true)
	{
		x = rand() % 10;
		y = rand() % 10;
		if (player.getField(x, y) == Field::Sunk || player.getField(x, y) == Field::Hostile)
			continue;
		else
		{
			if (player.getField(x, y) == Field::Boat)
				player.sinkShip(x, y);
			else
				player.setField(x, y, Field::Sunk);
			
            return true;
		}
	}
}

IPlayer* IPlayer::CreateComputerPlayer(Game& game)
{
    return new AIPlayer(game);
}
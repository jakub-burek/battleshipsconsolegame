#pragma once
#if !defined(__game_H__)
#define __game_H__

#include "Player.h"

class Game 
{
public:
	Game();
	~Game();

	void clearStart();
	void help();
	void play();
	void quitProgram();

private:
    IPlayer* playerOne;
    IPlayer* playerTwo;
};

#endif //__game_H__
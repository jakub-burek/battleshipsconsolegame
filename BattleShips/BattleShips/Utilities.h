#pragma once
#if !defined(__UTILITIES_H__)
#define __UTILITIES_H__

//Characters used to fill board:
// _ - unknown/empty field, S - empty hit field, B - not hit boat, H - hostile ship sunk
enum class Field : char
{
    Empty,
    Hostile,
    Sunk,
    Boat,
    Error
};

char fieldToCharacter(Field field);

const int shipsNumber = 14;

//Characters giving directions
enum class Direction : char
{
    Up,
    Right,
    Unknown
};

Direction charToDirection(char character);

int charToInt(char);
void clearInput();

#endif //__UTILITIES_H__
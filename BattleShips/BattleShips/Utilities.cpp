#include "Utilities.h"
#include <iostream>

char fieldToCharacter(Field field)
{
    switch (field)
    {
    case Field::Empty:
        return '_';
    case Field::Hostile:
        return 'H';
    case Field::Sunk:
        return 'S';
    case Field::Boat:
        return 'B';
    default:
    case Field::Error:
        return 'E';
    }
}

Direction charToDirection(char character)
{
    switch (character)
    {
    case 'u':
    case 'U':
        return Direction::Up;
    case 'r':
    case 'R':
        return Direction::Right;
    default:
        return Direction::Unknown;
    }
}

//Function changing character into
//row number on the board
int charToInt(char choice)
{
	return (tolower(choice) - 'a');
}

void clearInput()
{
    std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}
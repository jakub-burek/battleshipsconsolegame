#include "Player_base.h"

//Player constructor
PlayerBase::PlayerBase(Game& game) : 
    game(game)
{
    clearStart();
}

void PlayerBase::setup()
{
    //to override
}

void PlayerBase::print(IPlayer& player) const
{
    //to override
}

bool PlayerBase::attack(IPlayer& player)
{
    //to override
    return false;
}

//Method preparing player for a new round
void PlayerBase::clearStart()
{
    fleetCount = shipsNumber;
    for (int x = 0; x < 10; ++x)
    {
        for (int y = 0; y < 10; ++y)
            Board[x][y] = Field::Empty;
    }
}

Game& PlayerBase::getGame() const
{
    return game;
}

void PlayerBase::setName(std::string name)
{
    name = std::move(name);
}

const Field(&PlayerBase::GetBoard() const)[10][10]
{
    return Board;
}

//Method return players name, read only
const std::string& PlayerBase::getName() const
{
    return name;
}
//Method setting a ship on the board
bool PlayerBase::setOneShip(int x, int y, int lenght, Direction direction)
{
    if (x < 0 || x >= 10 || y < 0 || y >= 10 || lenght <= 0 || lenght > 5)
        return false;
    switch (direction)
    {
    case Direction::Up:
        if (y - lenght + 1 < 0)
            return false;
        for (int i = y + 1; i >= y - lenght; --i)
        {
            if (i >= 0 && i < 10)
            {
                if (x - 1 >= 0)
                    if (Board[x - 1][i] == Field::Boat)
                        return false;
                if (Board[x][i] == Field::Boat)
                    return false;
                if (x + 1 < 10)
                    if (Board[x + 1][i] == Field::Boat)
                        return false;
            }
        }
        for (int i = y; i >= y - lenght + 1; --i)
            Board[x][i] = Field::Boat;
        break;
    case Direction::Right:
        if (x + lenght - 1 >= 10)
            return false;
        for (int i = x - 1; i <= x + lenght; ++i)
        {
            if (i >= 0 && i < 10)
            {
                if (y - 1 >= 0)
                    if (Board[i][y - 1] == Field::Boat)
                        return false;
                if (Board[i][y] == Field::Boat)
                    return false;
                if (y + 1 < 10)
                    if (Board[i][y + 1] == Field::Boat)
                        return false;
            }
        }
        for (int i = x; i <= x + lenght - 1; ++i)
            Board[i][y] = Field::Boat;
        break;
    default:
        return false;
        break;
    }
    return true;
}
//Method sinks hit ship
bool PlayerBase::sinkShip(int x, int y)
{
    if (x < 0 || x >= 10 || y < 0 || y >= 10)
        return false;
    else
    {
        Board[x][y] = Field::Hostile;
        --fleetCount;
        return true;
    }
}
//Method returns field's content, read only
Field PlayerBase::getField(int x, int y) const
{
    if (x < 0 || x >= 10 || y < 0 || y >= 10)
        return Field::Error;
    else
        return Board[x][y];
}
//Method setting char on a filed
bool PlayerBase::setField(int x, int y, Field field)
{
    if (x < 0 || x >= 10 || y < 0 || y >= 10)
        return false;

    if (field == Field::Error)
        return false;

    Board[x][y] = field;
    return true;
}

//Method checking the score
bool PlayerBase::isLost() const
{
    return fleetCount <= 0;
}
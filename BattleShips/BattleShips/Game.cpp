#include "Game.h"
#include <iostream>
#include <string>

//game constructor
Game::Game() :
    playerOne(nullptr),
    playerTwo(nullptr)
{
	clearStart();
}
//game destructor deleting players involved in given round
Game::~Game()
{
	delete playerOne;
	delete playerTwo;
}
//Displays game and prepares the game for a new round
void Game::clearStart()
{
	while (true)
	{
		system("cls");
		std::cout << "	Welcome to Battleship!" << std::endl << std::endl;
		std::cout << "* To play with a friend press 0" << std::endl;
		std::cout << "* To play with a computer press 1" << std::endl;
		std::cout << "* For help press 2" << std::endl;
		std::cout << "* To quit press Q" << std::endl;
		char choice = std::cin.get();
        clearInput();
        std::cout << std::endl;
		if (choice == '0')
		{
            playerOne = IPlayer::CreateHumanPlayer(*this);
			playerTwo = IPlayer::CreateHumanPlayer(*this);
			break;
		}
		else if (choice == '1')
		{
			playerOne = IPlayer::CreateHumanPlayer(*this);
			playerTwo = IPlayer::CreateComputerPlayer(*this);
			break;
		}
		else if (choice == '2')
			help();
		else if (choice == 'Q' || choice == 'q')
			quitProgram();
	}
}
//Displays help
void Game::help()
{
	std::cout << "Battleship is a game for one or two players." << std::endl;
	std::cout << "You can play against a computer or a friend." << std::endl;
	std::cout << "If you want to play with a friend computer" << std::endl;
	std::cout << "is going to set up both of your boards and" << std::endl;
	std::cout << "you will take turns hitting enemy's board." << std::endl;
	std::cout << "If you want to play against a computer you" << std::endl;
	std::cout << "will set your board on your own." << std::endl;
	char choice = std::cin.get();
    clearInput();
    std::cout << std::endl;
	if (choice == 'q' || choice == 'Q')
		quitProgram();
	else
		system("cls");
}
//Controls the game
void Game::play()
{
    system("cls");
    playerOne->setup();
    system("cls");
    playerTwo->setup();
    system("cls");

	while (true)
	{
		playerOne->print(*playerTwo);
		playerOne->attack(*playerTwo);
		
        system("cls");
		
        if (playerTwo->isLost())
			break;
		
        playerTwo->print(*playerOne);
		playerTwo->attack(*playerOne);
		
        system("cls");

		if (playerOne->isLost())
			break;
	}

    const auto& winnerName = playerOne->isLost() ? playerTwo->getName() : playerOne->getName();
	std::cout << "Congratulations! " << winnerName << " You won!" << std::endl;
	
	system("pause");
    clearInput();
    clearStart();
}
//Allows you to stop the game at any given moment
void Game::quitProgram()
{
	exit(0);
}
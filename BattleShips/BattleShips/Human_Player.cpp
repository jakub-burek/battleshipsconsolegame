#include "Player_base.h"
#include "Game.h"
#include "Utilities.h"
#include <iostream>
#include <string>

class HumanPlayer final : public PlayerBase
{
public:
    HumanPlayer(Game& game);
    virtual ~HumanPlayer() override final = default;

    virtual void setup() override final;
    virtual void print(IPlayer& player) const override final;
    virtual bool attack(IPlayer& player) override final;
	void printWhileSetup();
};

//Human Player constructor, allows to set players name. 
HumanPlayer::HumanPlayer(Game& game) : 
    PlayerBase(game)
{
}

void HumanPlayer::printWhileSetup()
{
	std::cout << "____ME_____" << std::endl;
	std::cout << " 0123456789" << std::endl;
	char tmp = 'A';
	const auto board = GetBoard();

	for (int y = 0; y < 10; ++y)
	{
		std::cout << tmp++;
		for (int x = 0; x < 10; ++x)
			std::cout << fieldToCharacter(board[x][y]);
		std::cout << std::endl;
	}
}

void HumanPlayer::setup()
{
    std::cout << "Type in your name" << std::endl;
    std::string name;
    std::cin >> name;
    setName(std::move(name));
    clearInput();

    system("cls");

    std::cout << "Set your ships in place!" << std::endl;
    std::cout << "To do that type in field and direction!" << std::endl;
    std::cout << "For example: D2 u (for up) or B4 r (for right)." << std::endl;
    std::cout << "You will set accordingly: 5, 4, 3 and 2-mast ships." << std::endl;
    char yChar, direction;
    int x, y;

    for (int i = 5; i > 1; --i)
    {
        while (true)
        {
			printWhileSetup();
            std::cout << "Set " << i << "-mast ship (e.g. D7 u)" << std::endl;
            std::cin >> yChar >> x >> direction;
            clearInput();
            y = charToInt(yChar);

            if (setOneShip(x, y, i, charToDirection(direction)) == true)
                break;
        }
    }
}

//Method printing enemy's and player's board
void HumanPlayer::print(IPlayer& player) const
{
    const auto board = GetBoard();
	
	std::cout << "___ENEMY___________ME_____" << std::endl;
	char tmp = 'A';
	std::cout << " 0123456789    0123456789" << std::endl;
	
    for (int y = 0; y < 10; ++y)
	{
		std::cout << tmp;
		for (int x = 0; x < 10; ++x)
		{
            const auto field = player.getField(x, y);

            if (field == Field::Sunk || field == Field::Hostile)
                std::cout << fieldToCharacter(field);
            else
                std::cout << fieldToCharacter(Field::Empty);
		}
		std::cout << "   " << tmp++;
		
        for (int x = 0; x < 10; ++x)
			std::cout << fieldToCharacter(board[x][y]);

		std::cout << std::endl;
	}
}
//Method allowing player to target enemy's ships
bool HumanPlayer::attack(IPlayer& player)
{
	char yChar;
	int x, y;
	while (true)
	{
		std::cout << "Choose your move (e.g. D7)" << std::endl;
		std::cin >> yChar >> x;
        clearInput(); 
        y = charToInt(yChar);
		
        if (x < 0 || x >= 10 || y < 0 || y >= 10 || player.getField(x, y) == Field::Sunk)
			continue;
		else
		{
			if (player.getField(x, y) == Field::Boat)
				player.sinkShip(x, y);
			else
				player.setField(x, y, Field::Sunk);
			return true;
		}
	}
	return false;
}

IPlayer* IPlayer::CreateHumanPlayer(Game& game)
{
    return new HumanPlayer(game);
}